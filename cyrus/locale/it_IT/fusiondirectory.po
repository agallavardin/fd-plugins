# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR FusionDirectory Project
# This file is distributed under the same license as the FusionDirectory package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# fusiondirectory <contact@fusiondirectory.org>, 2020
# Paola <paola.penati@fusiondirectory.org>, 2020
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: FusionDirectory VERSION\n"
"Report-Msgid-Bugs-To: bugs@fusiondirectory.org\n"
"POT-Creation-Date: 2020-05-15 09:03+0000\n"
"PO-Revision-Date: 2018-08-13 19:51+0000\n"
"Last-Translator: Paola <paola.penati@fusiondirectory.org>, 2020\n"
"Language-Team: Italian (Italy) (https://www.transifex.com/fusiondirectory/teams/12202/it_IT/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: it_IT\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: admin/systems/services/cyrus/class_serviceCyrus.inc:27
#: admin/systems/services/cyrus/class_serviceCyrus.inc:28
msgid "Cyrus (IMAP/POP3)"
msgstr "Cyrus (IMAP/POP3)"

#: admin/systems/services/cyrus/class_serviceCyrus.inc:28
msgid "Services"
msgstr "Servizi"

#: admin/systems/services/cyrus/class_serviceCyrus.inc:45
msgid "Connection"
msgstr "Connessione"

#: admin/systems/services/cyrus/class_serviceCyrus.inc:48
msgid "Connect URL for Cyrus server"
msgstr "Connetere all'URL del server Cyrus"

#: admin/systems/services/cyrus/class_serviceCyrus.inc:52
#: admin/systems/services/cyrus/class_serviceCyrus.inc:93
msgid "Hostname"
msgstr "Nome dell'host"

#: admin/systems/services/cyrus/class_serviceCyrus.inc:52
msgid "Hostname of the Cyrus server"
msgstr "Nome del server Cyrus"

#: admin/systems/services/cyrus/class_serviceCyrus.inc:56
#: admin/systems/services/cyrus/class_serviceCyrus.inc:97
msgid "Port"
msgstr "Porta"

#: admin/systems/services/cyrus/class_serviceCyrus.inc:56
msgid "Port number on which Cyrus server should be contacted"
msgstr "Numero della porta sulla quale il server Cyrus sarà contattato"

#: admin/systems/services/cyrus/class_serviceCyrus.inc:61
#: admin/systems/services/cyrus/class_serviceCyrus.inc:102
msgid "Option"
msgstr "Opzione"

#: admin/systems/services/cyrus/class_serviceCyrus.inc:61
msgid "Options for contacting Cyrus server"
msgstr "Opzioni per la connessione verso il server Cyrus"

#: admin/systems/services/cyrus/class_serviceCyrus.inc:66
msgid "Valide certificats"
msgstr "Certificati validi"

#: admin/systems/services/cyrus/class_serviceCyrus.inc:66
msgid "Weither or not to validate server certificate on connexion"
msgstr "Convalidare oppure no il certificato del server alla connessione"

#: admin/systems/services/cyrus/class_serviceCyrus.inc:76
msgid "Admin user"
msgstr "Amministratore"

#: admin/systems/services/cyrus/class_serviceCyrus.inc:76
msgid "Imap server admin user"
msgstr "Amministratore del server Imap"

#: admin/systems/services/cyrus/class_serviceCyrus.inc:80
msgid "Password"
msgstr "Password"

#: admin/systems/services/cyrus/class_serviceCyrus.inc:80
msgid "Admin user password"
msgstr "Password dell'amministratore "

#: admin/systems/services/cyrus/class_serviceCyrus.inc:86
msgid "Sieve"
msgstr "Setaccio"

#: admin/systems/services/cyrus/class_serviceCyrus.inc:89
msgid "Sieve connect URL for Cyrus server"
msgstr "URL di connessione Sieve per il server Cyrus"

#: admin/systems/services/cyrus/class_serviceCyrus.inc:93
msgid "Hostname of the Cyrus sieve server"
msgstr "Nome host del server Cyrus"

#: admin/systems/services/cyrus/class_serviceCyrus.inc:97
msgid "Port number on which Cyrus sieve server should be contacted"
msgstr "Numero della porta sulla quale sieve Cyrus server sarà contattato"

#: admin/systems/services/cyrus/class_serviceCyrus.inc:102
msgid "Options for contacting Cyrus sieve server"
msgstr "Opzioni per contattare il server Sieve di Cyrus"

#: admin/systems/services/cyrus/class_serviceCyrus.inc:113
msgid "Settings"
msgstr "Impostazioni"

#: admin/systems/services/cyrus/class_serviceCyrus.inc:116
msgid "Use UNIX style"
msgstr "Usa lo stile UNIX"

#: admin/systems/services/cyrus/class_serviceCyrus.inc:116
msgid "Determines if \"foo/bar\" or \"foo.bar\" should be used as namespaces in IMAP"
msgstr ""
"Determina se \"foo/bar\" o \"foo.bar\" devono essere utilizzati come spazi "
"dei nomi in IMAP"

#: admin/systems/services/cyrus/class_serviceCyrus.inc:122
msgid "Autocreate folders"
msgstr "Crea cartelle automaticamente"

#: admin/systems/services/cyrus/class_serviceCyrus.inc:122
msgid ""
"List of personal IMAP folders that should be created along initial account "
"creation."
msgstr ""
"Elenco delle cartelle IMAP personali che dovranno essere create durante la "
"creazione iniziale dell'account."

#: personal/mail/mail-methods/class_mail-methods-cyrus.inc:231
msgid "IMAP error"
msgstr "Errore IMAP"

#: personal/mail/mail-methods/class_mail-methods-cyrus.inc:231
#, php-format
msgid "Cannot modify IMAP mailbox quota: %s"
msgstr ""
"Impossibile modificare la quota della casella di posta elettronica IMAP :%s"

#: personal/mail/mail-methods/class_mail-methods-cyrus.inc:307
msgid "Mail info"
msgstr "informazioni Mail"

#: personal/mail/mail-methods/class_mail-methods-cyrus.inc:308
#, php-format
msgid ""
"LDAP entry has been removed but cyrus mailbox (%s) is kept.\n"
"Please delete it manually!"
msgstr ""
"L'entry LDAP è stata eliminata, ma la casella di posta elettronica (%s) è ancora presente in cyrus.\n"
"Elimina manualmente !"

#: personal/mail/mail-methods/class_mail-methods-cyrus.inc:403
#: personal/mail/mail-methods/class_mail-methods-cyrus.inc:462
msgid "The module imap_getacl is not implemented!"
msgstr "Il modulo imap_getacl non è implementato !"

#: personal/mail/mail-methods/class_mail-methods-cyrus.inc:535
#, php-format
msgid "Cannot retrieve SIEVE script: %s"
msgstr "Impossibile recuperare lo script SIEVE : %s"

#: personal/mail/mail-methods/class_mail-methods-cyrus.inc:603
#, php-format
msgid "Cannot store SIEVE script: %s"
msgstr "Impossibile memorizzare lo script SIEVE : %s"

#: personal/mail/mail-methods/class_mail-methods-cyrus.inc:610
#, php-format
msgid "Cannot activate SIEVE script: %s"
msgstr "Impossibile attivare lo script SIEVE : %s"
