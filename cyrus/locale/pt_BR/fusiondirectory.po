# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR FusionDirectory Project
# This file is distributed under the same license as the FusionDirectory package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# fusiondirectory <contact@fusiondirectory.org>, 2018
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: FusionDirectory VERSION\n"
"Report-Msgid-Bugs-To: bugs@fusiondirectory.org\n"
"POT-Creation-Date: 2020-05-15 09:03+0000\n"
"PO-Revision-Date: 2018-08-13 19:51+0000\n"
"Last-Translator: fusiondirectory <contact@fusiondirectory.org>, 2018\n"
"Language-Team: Portuguese (Brazil) (https://www.transifex.com/fusiondirectory/teams/12202/pt_BR/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: pt_BR\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: admin/systems/services/cyrus/class_serviceCyrus.inc:27
#: admin/systems/services/cyrus/class_serviceCyrus.inc:28
msgid "Cyrus (IMAP/POP3)"
msgstr "Cyrus (IMAP/POP3)"

#: admin/systems/services/cyrus/class_serviceCyrus.inc:28
msgid "Services"
msgstr "Serviços"

#: admin/systems/services/cyrus/class_serviceCyrus.inc:45
msgid "Connection"
msgstr ""

#: admin/systems/services/cyrus/class_serviceCyrus.inc:48
msgid "Connect URL for Cyrus server"
msgstr "Conectar URL pelo servidor Cyrus"

#: admin/systems/services/cyrus/class_serviceCyrus.inc:52
#: admin/systems/services/cyrus/class_serviceCyrus.inc:93
msgid "Hostname"
msgstr "Nome do host"

#: admin/systems/services/cyrus/class_serviceCyrus.inc:52
msgid "Hostname of the Cyrus server"
msgstr "Nome do host do servidor Cyrus"

#: admin/systems/services/cyrus/class_serviceCyrus.inc:56
#: admin/systems/services/cyrus/class_serviceCyrus.inc:97
msgid "Port"
msgstr "Porta"

#: admin/systems/services/cyrus/class_serviceCyrus.inc:56
msgid "Port number on which Cyrus server should be contacted"
msgstr "Número da porta a qual Cyrus deve se conectar"

#: admin/systems/services/cyrus/class_serviceCyrus.inc:61
#: admin/systems/services/cyrus/class_serviceCyrus.inc:102
msgid "Option"
msgstr "Opção"

#: admin/systems/services/cyrus/class_serviceCyrus.inc:61
msgid "Options for contacting Cyrus server"
msgstr "Opções para contactar servidor Cyrus"

#: admin/systems/services/cyrus/class_serviceCyrus.inc:66
msgid "Valide certificats"
msgstr "Validar certificados"

#: admin/systems/services/cyrus/class_serviceCyrus.inc:66
msgid "Weither or not to validate server certificate on connexion"
msgstr "Validar ou não os certificados dos servidores durante a conexão"

#: admin/systems/services/cyrus/class_serviceCyrus.inc:76
msgid "Admin user"
msgstr "Usuário admin"

#: admin/systems/services/cyrus/class_serviceCyrus.inc:76
msgid "Imap server admin user"
msgstr "usuário administrador do servidor Imap"

#: admin/systems/services/cyrus/class_serviceCyrus.inc:80
msgid "Password"
msgstr "Senha"

#: admin/systems/services/cyrus/class_serviceCyrus.inc:80
msgid "Admin user password"
msgstr "Senha do usuário admin"

#: admin/systems/services/cyrus/class_serviceCyrus.inc:86
msgid "Sieve"
msgstr ""

#: admin/systems/services/cyrus/class_serviceCyrus.inc:89
msgid "Sieve connect URL for Cyrus server"
msgstr "Conectar filtro URL para servidor Cyrus"

#: admin/systems/services/cyrus/class_serviceCyrus.inc:93
msgid "Hostname of the Cyrus sieve server"
msgstr "Nome do servidos para o filtro do servidor Cyrus"

#: admin/systems/services/cyrus/class_serviceCyrus.inc:97
msgid "Port number on which Cyrus sieve server should be contacted"
msgstr "Número da porta onde o filtro do servidor Cyrus deve ser contactado"

#: admin/systems/services/cyrus/class_serviceCyrus.inc:102
msgid "Options for contacting Cyrus sieve server"
msgstr "Opções para contactar filtro do servidor Cyrus"

#: admin/systems/services/cyrus/class_serviceCyrus.inc:113
msgid "Settings"
msgstr ""

#: admin/systems/services/cyrus/class_serviceCyrus.inc:116
msgid "Use UNIX style"
msgstr ""

#: admin/systems/services/cyrus/class_serviceCyrus.inc:116
msgid "Determines if \"foo/bar\" or \"foo.bar\" should be used as namespaces in IMAP"
msgstr ""

#: admin/systems/services/cyrus/class_serviceCyrus.inc:122
msgid "Autocreate folders"
msgstr ""

#: admin/systems/services/cyrus/class_serviceCyrus.inc:122
msgid ""
"List of personal IMAP folders that should be created along initial account "
"creation."
msgstr ""

#: personal/mail/mail-methods/class_mail-methods-cyrus.inc:231
msgid "IMAP error"
msgstr "Erro de IMAP"

#: personal/mail/mail-methods/class_mail-methods-cyrus.inc:231
#, php-format
msgid "Cannot modify IMAP mailbox quota: %s"
msgstr "Impossível modificar caixa de e-mail IMAP :%s"

#: personal/mail/mail-methods/class_mail-methods-cyrus.inc:307
msgid "Mail info"
msgstr "Informação de correio"

#: personal/mail/mail-methods/class_mail-methods-cyrus.inc:308
#, php-format
msgid ""
"LDAP entry has been removed but cyrus mailbox (%s) is kept.\n"
"Please delete it manually!"
msgstr ""
"Entrada LDAP foi removida mas a caixa de correio cyrus (%s) foi mantida. Por"
" favor, delete manualmente!"

#: personal/mail/mail-methods/class_mail-methods-cyrus.inc:403
#: personal/mail/mail-methods/class_mail-methods-cyrus.inc:462
msgid "The module imap_getacl is not implemented!"
msgstr "O modulo imap_getacl não está implementado!"

#: personal/mail/mail-methods/class_mail-methods-cyrus.inc:535
#, php-format
msgid "Cannot retrieve SIEVE script: %s"
msgstr "Não é possível recuperar script SIEVE : %s"

#: personal/mail/mail-methods/class_mail-methods-cyrus.inc:603
#, php-format
msgid "Cannot store SIEVE script: %s"
msgstr "Não é possível armazenar script SIEVE:%s"

#: personal/mail/mail-methods/class_mail-methods-cyrus.inc:610
#, php-format
msgid "Cannot activate SIEVE script: %s"
msgstr "Não é possível ativar script SIEVE:%s"
