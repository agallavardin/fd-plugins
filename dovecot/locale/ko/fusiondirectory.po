# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR FusionDirectory Project
# This file is distributed under the same license as the FusionDirectory package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# fusiondirectory <contact@fusiondirectory.org>, 2018
# Choi Chris <chulwon.choi@gmail.com>, 2019
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: FusionDirectory VERSION\n"
"Report-Msgid-Bugs-To: bugs@fusiondirectory.org\n"
"POT-Creation-Date: 2020-05-15 09:03+0000\n"
"PO-Revision-Date: 2018-08-13 19:53+0000\n"
"Last-Translator: Choi Chris <chulwon.choi@gmail.com>, 2019\n"
"Language-Team: Korean (https://www.transifex.com/fusiondirectory/teams/12202/ko/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ko\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: admin/systems/services/dovecot/class_serviceDovecot.inc:26
#: admin/systems/services/dovecot/class_serviceDovecot.inc:27
msgid "Dovecot (IMAP/POP3)"
msgstr "Dovecot (IMAP/POP3)"

#: admin/systems/services/dovecot/class_serviceDovecot.inc:27
msgid "Services"
msgstr "서비스"

#: admin/systems/services/dovecot/class_serviceDovecot.inc:42
msgid "Dovecot connection"
msgstr "Dovecot 연결"

#: admin/systems/services/dovecot/class_serviceDovecot.inc:45
msgid "Connect URL for Dovecot server"
msgstr "Dovecot 서버 연결 URL"

#: admin/systems/services/dovecot/class_serviceDovecot.inc:49
msgid "Hostname"
msgstr "호스트명"

#: admin/systems/services/dovecot/class_serviceDovecot.inc:49
msgid "Hostname of the Dovecot server"
msgstr "Dovecot 서버의 호스트명"

#: admin/systems/services/dovecot/class_serviceDovecot.inc:53
msgid "Port"
msgstr "포트"

#: admin/systems/services/dovecot/class_serviceDovecot.inc:53
msgid "Port number on which Dovecot server should be contacted"
msgstr "Dovecot 서버에 접속해야 하는 포트 번호"

#: admin/systems/services/dovecot/class_serviceDovecot.inc:58
msgid "Option"
msgstr "옵션"

#: admin/systems/services/dovecot/class_serviceDovecot.inc:58
msgid "Options for contacting Dovecot server"
msgstr "Dovecot 서버 접속 옵션"

#: admin/systems/services/dovecot/class_serviceDovecot.inc:63
msgid "Validate TLS/SSL Certificates"
msgstr "TLS / SSL 인증서 확인"

#: admin/systems/services/dovecot/class_serviceDovecot.inc:63
msgid "Weither or not to validate server certificates on connection"
msgstr "연결시 서버 인증서의 유효성을 검사 여부"

#: admin/systems/services/dovecot/class_serviceDovecot.inc:75
msgid "Master credentials"
msgstr "마스터 자격증명"

#: admin/systems/services/dovecot/class_serviceDovecot.inc:78
msgid "Admin user"
msgstr "관리자"

#: admin/systems/services/dovecot/class_serviceDovecot.inc:78
msgid "Dovecot server admin user"
msgstr "Dovecot 서버 관리자"

#: admin/systems/services/dovecot/class_serviceDovecot.inc:82
msgid "Password"
msgstr "패스워드"

#: admin/systems/services/dovecot/class_serviceDovecot.inc:82
msgid "Admin user password"
msgstr "관리자 패스워드"

#: admin/systems/services/dovecot/class_serviceDovecot.inc:86
msgid "Mail directory"
msgstr "메일 디렉토리"

#: admin/systems/services/dovecot/class_serviceDovecot.inc:86
msgid "Path to the directory in which user maildirs must be created"
msgstr "사용자 maildir을 작성해야하는 디렉토리 경로"

#: admin/systems/services/dovecot/class_serviceDovecot.inc:93
msgid "Options"
msgstr "옵션"

#: admin/systems/services/dovecot/class_serviceDovecot.inc:96
msgid "Create the user folder via Argonaut"
msgstr "Argonaut를 통해 사용자 폴더 만들기"

#: admin/systems/services/dovecot/class_serviceDovecot.inc:96
msgid "Use argonaut to create the user mailbox on the Dovecot server"
msgstr "argonaut를 사용하여 Dovecot 서버에 사용자 사서함을 만듭니다."

#: personal/mail/mail-methods/class_mail-methods-dovecot.inc:185
#, php-format
msgid "Error while trying to contact Dovecot server through Argonaut: %s"
msgstr "Argonaut를 통해 Dovecot 서버에 접속하는 동안 오류가 발생했습니다: %s"
