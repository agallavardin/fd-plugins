# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR FusionDirectory Project
# This file is distributed under the same license as the FusionDirectory package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# fusiondirectory <contact@fusiondirectory.org>, 2018
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: FusionDirectory VERSION\n"
"Report-Msgid-Bugs-To: bugs@fusiondirectory.org\n"
"POT-Creation-Date: 2020-05-15 09:03+0000\n"
"PO-Revision-Date: 2018-08-13 19:54+0000\n"
"Last-Translator: fusiondirectory <contact@fusiondirectory.org>, 2018\n"
"Language-Team: Russian (https://www.transifex.com/fusiondirectory/teams/12202/ru/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ru\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);\n"

#: admin/freeradius/class_freeradiusGroup.inc:30
#: personal/freeradius/class_freeradiusAccount.inc:31
msgid "Freeradius"
msgstr "Freeradius"

#: admin/freeradius/class_freeradiusGroup.inc:31
#: personal/freeradius/class_freeradiusAccount.inc:32
msgid "This Plugin is for Radius accounting based in FreeRadius"
msgstr ""

#: admin/freeradius/class_freeradiusGroup.inc:48
#: personal/freeradius/class_freeradiusAccount.inc:53
msgid "Support 802.1x"
msgstr "Поддержка 802.1x"

#: admin/freeradius/class_freeradiusGroup.inc:51
#: personal/freeradius/class_freeradiusAccount.inc:56
msgid "Tunnel medium type"
msgstr ""

#: admin/freeradius/class_freeradiusGroup.inc:52
#: personal/freeradius/class_freeradiusAccount.inc:56
msgid "Name of the tunnel medium type"
msgstr ""

#: admin/freeradius/class_freeradiusGroup.inc:58
#: personal/freeradius/class_freeradiusAccount.inc:61
msgid "Tunnel type"
msgstr "Тип туннеля"

#: admin/freeradius/class_freeradiusGroup.inc:59
#: personal/freeradius/class_freeradiusAccount.inc:61
msgid "Name of the tunnel type"
msgstr "Название типа туннеля"

#: admin/freeradius/class_freeradiusGroup.inc:65
#: personal/freeradius/class_freeradiusAccount.inc:66
msgid "VLAN id"
msgstr "VLAN id"

#: admin/freeradius/class_freeradiusGroup.inc:66
#: personal/freeradius/class_freeradiusAccount.inc:66
msgid "VLAN identifier"
msgstr "Идентификатор VLAN"

#: personal/freeradius/class_freeradiusAccount.inc:71
msgid "Expiration date"
msgstr "Дата окончания действия"

#: personal/freeradius/class_freeradiusAccount.inc:71
msgid "Date of the expiration account"
msgstr ""

#: personal/freeradius/class_freeradiusAccount.inc:78
#: personal/freeradius/class_freeradiusAccount.inc:82
msgid "Groups"
msgstr "Группы"

#: personal/freeradius/class_freeradiusAccount.inc:83
msgid "FreeRadius Group"
msgstr ""

#: personal/freeradius/class_freeradiusAccount.inc:92
msgid "User preferences"
msgstr ""

#: personal/freeradius/class_freeradiusAccount.inc:95
msgid "Protocol"
msgstr "Протокол"

#: personal/freeradius/class_freeradiusAccount.inc:100
msgid "IP Address"
msgstr "IP адрес"

#: personal/freeradius/class_freeradiusAccount.inc:105
msgid "IP Netmask"
msgstr "сетевая маска IP"

#: personal/freeradius/class_freeradiusAccount.inc:110
msgid "Framed-MTU"
msgstr ""

#: personal/freeradius/class_freeradiusAccount.inc:117
msgid "Compression"
msgstr "Сжатие"

#: personal/freeradius/class_freeradiusAccount.inc:122
msgid "Service type"
msgstr ""

#: personal/freeradius/class_freeradiusAccount.inc:127
msgid "Session Timeout"
msgstr ""

#: personal/freeradius/class_freeradiusAccount.inc:134
msgid "Idle Timeout"
msgstr ""

#: personal/freeradius/class_freeradiusAccount.inc:141
msgid "Port limit"
msgstr ""
