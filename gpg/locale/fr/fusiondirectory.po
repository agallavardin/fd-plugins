# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR FusionDirectory Project
# This file is distributed under the same license as the FusionDirectory package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# fusiondirectory <contact@fusiondirectory.org>, 2018
# MCMic, 2019
# Benoit Mortier <benoit.mortier@fusiondirectory.org>, 2019
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: FusionDirectory VERSION\n"
"Report-Msgid-Bugs-To: bugs@fusiondirectory.org\n"
"POT-Creation-Date: 2020-05-15 09:03+0000\n"
"PO-Revision-Date: 2018-08-13 19:54+0000\n"
"Last-Translator: Benoit Mortier <benoit.mortier@fusiondirectory.org>, 2019\n"
"Language-Team: French (https://www.transifex.com/fusiondirectory/teams/12202/fr/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: fr\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: addons/gpg/class_pgpServerInfo.inc:27 addons/gpg/class_pgpServerInfo.inc:28
#: addons/gpg/class_pgpServerInfo.inc:35 addons/gpg/class_pgpServerInfo.inc:52
msgid "GPG server info"
msgstr "Informations du serveur GPG"

#: addons/gpg/class_pgpServerInfo.inc:56
msgid "Software"
msgstr "Logiciel"

#: addons/gpg/class_pgpServerInfo.inc:56
msgid "Which software this GPG key server is running"
msgstr "Quel logiciel ce serveur de clés GPG utilise"

#: addons/gpg/class_pgpServerInfo.inc:61
msgid "GPG version"
msgstr "Version de GPG"

#: addons/gpg/class_pgpServerInfo.inc:61
msgid "Which version of GPG this key server is running"
msgstr "Version de GPG que ce serveur de clés utilise"

#: addons/gpg/class_pgpServerInfo.inc:66
msgid "Branch in which keys are stored"
msgstr "Branche où sont stockées les clés GPG"

#: addons/gpg/class_pgpServerInfo.inc:70
msgid "Keys RDN"
msgstr "Branche des clés"

#: addons/gpg/class_pgpServerInfo.inc:70
msgid "Branch under base in which keys are stored"
msgstr "Branche où sont stockées les clés GPG"

#: personal/gpg/class_gpgAccount.inc:28
msgid "GPG"
msgstr "GPG"

#: personal/gpg/class_gpgAccount.inc:29
msgid "Edit user's GPG IDs"
msgstr "Modifier les identifiants GPG de l'utilisateur"

#: personal/gpg/class_gpgAccount.inc:44
msgid "GPG keys"
msgstr "Clés GPG"

#: personal/gpg/class_gpgAccount.inc:48
msgid "GPG keys of this user"
msgstr "Liste des clés GPG de cet utilisateur"

#: personal/gpg/pgpKeySelect/class_pgpKeySelect.inc:27
msgid "PGP Key selection"
msgstr "Sélection de la clé PGP"

#: personal/gpg/pgpKeySelect/class_pgpKeySelect.inc:28
msgid "PGP Key selection for a user"
msgstr "Sélection de la clé PGP pour un utilisateur"

#: personal/gpg/pgpKeySelect/class_pgpKeySelect.inc:30
msgid "PGP Key"
msgstr "Clé PGP"

#: personal/gpg/pgpKeySelect/class_pgpKeySelect.inc:31
msgid "PGP Key information"
msgstr "Informations clés sur PGP"

#: personal/gpg/pgpKeySelect/class_pgpKeySelect.inc:66
msgid "Configuration error"
msgstr "Erreur de configuration"

#: personal/gpg/pgpKeySelect/class_pgpKeySelect.inc:66
msgid "You need to configure GPG base dn through the addons section first"
msgstr ""
"Vous devez configurer d'abord le DN de base de GPG, dans la section "
"«Configuration»"

#: personal/gpg/pgpKeySelect/class_pgpKeySelect.inc:89
msgid "Cert ID"
msgstr "Cert ID"

#: personal/gpg/pgpKeySelect/class_pgpKeySelect.inc:90
msgid "Key ID"
msgstr "ID de la clef"

#: personal/gpg/pgpKeySelect/class_pgpKeySelect.inc:91
msgid "User ID"
msgstr "ID de l'utilisateur"

#: personal/gpg/pgpKeySelect/class_pgpKeySelect.inc:92
msgid "Creation time"
msgstr "Date de création"

#: personal/gpg/pgpKeySelect/class_pgpKeySelect.inc:93
msgid "Expiration"
msgstr "Date d'expiration"

#: personal/gpg/pgpKeySelect/class_pgpKeySelect.inc:94
msgid "Algorithm"
msgstr "Algorithme"

#: personal/gpg/pgpKeySelect/class_pgpKeySelect.inc:95
msgid "Size"
msgstr "Taille"

#: personal/gpg/pgpKeySelect/class_pgpKeySelect.inc:96
msgid "Revoked"
msgstr "Révoquée"

#: personal/gpg/pgpKeySelect/class_pgpKeySelect.inc:97
msgid "Disabled"
msgstr "Désactivée"
