# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR FusionDirectory Project
# This file is distributed under the same license as the FusionDirectory package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# fusiondirectory <contact@fusiondirectory.org>, 2018
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: FusionDirectory VERSION\n"
"Report-Msgid-Bugs-To: bugs@fusiondirectory.org\n"
"POT-Creation-Date: 2020-05-15 09:03+0000\n"
"PO-Revision-Date: 2018-08-13 20:01+0000\n"
"Last-Translator: fusiondirectory <contact@fusiondirectory.org>, 2018\n"
"Language-Team: Dutch (https://www.transifex.com/fusiondirectory/teams/12202/nl/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: nl\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: admin/systems/services/quota/class_serviceQuota.inc:27
#: admin/systems/services/quota/class_serviceQuota.inc:28
msgid "Quota service"
msgstr "Quotaservice"

#: admin/systems/services/quota/class_serviceQuota.inc:28
msgid "Services"
msgstr "Services"

#: admin/systems/services/quota/class_serviceQuota.inc:41
msgid "Shares"
msgstr "Shares"

#: admin/systems/services/quota/class_serviceQuota.inc:45
msgid "Quotas for the shares this server hosts"
msgstr "Quotas voor de shares die deze server hosts"

#: admin/systems/services/quota/class_serviceQuota.inc:49
#: personal/quota/class_quotaAccount.inc:129
msgid "Device"
msgstr "Toestel"

#: admin/systems/services/quota/class_serviceQuota.inc:49
msgid "Device concerned by this quota"
msgstr "Toestel betrokken bij dit quota"

#: admin/systems/services/quota/class_serviceQuota.inc:53
msgid "Quota block size"
msgstr "Quota-blokgrootte"

#: admin/systems/services/quota/class_serviceQuota.inc:53
msgid "Quota block size on this share (usually 1024)"
msgstr "Quota-blokgrootte op deze share (meestal 1024)"

#: admin/systems/services/quota/class_serviceQuota.inc:62
msgid "Comment"
msgstr "Opmerking"

#: admin/systems/services/quota/class_serviceQuota.inc:62
msgid "Short comment about this quota"
msgstr "Korte uitleg over dit quotum"

#: admin/systems/services/quota/class_serviceQuota.inc:77
msgid "Quota warning message"
msgstr "Quota-waarschuwingsboodschap"

#: admin/systems/services/quota/class_serviceQuota.inc:80
msgid "Charset"
msgstr "Charset"

#: admin/systems/services/quota/class_serviceQuota.inc:80
msgid "Charset use for the message"
msgstr "Charsetgebruik voor de boodschap"

#: admin/systems/services/quota/class_serviceQuota.inc:84
msgid "Mail command"
msgstr "E-mailopdracht"

#: admin/systems/services/quota/class_serviceQuota.inc:84
msgid "Command allowing the server to send mails"
msgstr "Opdracht die de server toelaat om mails te versturen"

#: admin/systems/services/quota/class_serviceQuota.inc:88
msgid "Support contact"
msgstr "Contacteer ondersteuning"

#: admin/systems/services/quota/class_serviceQuota.inc:88
msgid "Email address to contact the administrator"
msgstr "E-mailadres om de administrator te contacteren"

#: admin/systems/services/quota/class_serviceQuota.inc:92
msgid "From"
msgstr "Van"

#: admin/systems/services/quota/class_serviceQuota.inc:92
msgid "Email from which the email is sent"
msgstr "email vanwaar de mail is verzonden"

#: admin/systems/services/quota/class_serviceQuota.inc:96
msgid "CC"
msgstr "CC"

#: admin/systems/services/quota/class_serviceQuota.inc:96
msgid "Send a copy of this email to"
msgstr "Verstuur een kopie van deze mail naar"

#: admin/systems/services/quota/class_serviceQuota.inc:100
msgid "Subject"
msgstr "Onderwerp"

#: admin/systems/services/quota/class_serviceQuota.inc:100
msgid "Subject of the sent warning email"
msgstr "Onderwerp van de verzonden e-mailwaarschuwing"

#: admin/systems/services/quota/class_serviceQuota.inc:104
msgid "Content"
msgstr "Inhoud"

#: admin/systems/services/quota/class_serviceQuota.inc:104
msgid "Content of the sent email"
msgstr "Inhoud van de verzonden e-mail"

#: admin/systems/services/quota/class_serviceQuota.inc:108
msgid "Signature"
msgstr "Handtekening"

#: admin/systems/services/quota/class_serviceQuota.inc:108
msgid "Signature to put at the end of the mail"
msgstr "Handtekening om aan het einde van de mail te zetten"

#: admin/systems/services/quota/class_serviceQuota.inc:114
msgid "LDAP message support"
msgstr "LDAP boodschapondersteuning"

#: admin/systems/services/quota/class_serviceQuota.inc:117
msgid "LDAP server"
msgstr "LDAP server"

#: admin/systems/services/quota/class_serviceQuota.inc:117
msgid "The LDAP server to bind to"
msgstr "De LDAP server waarnaar te binden"

#: admin/systems/services/quota/class_serviceQuota.inc:121
msgid "Bind user dn"
msgstr "Bind gebruiker-dn"

#: admin/systems/services/quota/class_serviceQuota.inc:121
msgid "The user dn to use for bind"
msgstr "De gebruiker-dn voor bind"

#: admin/systems/services/quota/class_serviceQuota.inc:125
msgid "Bind user password"
msgstr "Bind gebruikerpaswoord"

#: admin/systems/services/quota/class_serviceQuota.inc:125
msgid "The password of the user used for bind"
msgstr "Het paswoord van de gebruiker voor de bind"

#: admin/systems/services/quota/class_serviceQuota.inc:158
msgid "Warning"
msgstr "Waarschuwing"

#: admin/systems/services/quota/class_serviceQuota.inc:158
#, php-format
msgid "Invalid value in encodings : %s"
msgstr "Ongeldige waarde in coderingen : %s"

#: admin/systems/services/quota/class_serviceQuota.inc:174
#, php-format
msgid "(%s by %s)"
msgstr "(%s op %s)"

#: personal/quota/class_quotaAccount.inc:99
msgid "Quota"
msgstr "Quota"

#: personal/quota/class_quotaAccount.inc:100
msgid "Plugin for quota support"
msgstr "Plugin voor quota support"

#: personal/quota/class_quotaAccount.inc:120
msgid "Quota information"
msgstr "Quota informatie"

#: personal/quota/class_quotaAccount.inc:125
msgid "Quota information for this user"
msgstr "Quota informatie voor deze gebruiker"

#: personal/quota/class_quotaAccount.inc:129
msgid "Device this quota is for"
msgstr "Toestel waarvoor dit quota is toegewezen"

#: personal/quota/class_quotaAccount.inc:133
msgid "Block soft limit"
msgstr "Blok softlimiet"

#: personal/quota/class_quotaAccount.inc:133
msgid "Soft limit for the block the user can use"
msgstr "Softlimiet voor het blok dat de gebruiker kan toepassen"

#: personal/quota/class_quotaAccount.inc:142
msgid "Block hard limit"
msgstr "Blok hardlimiet"

#: personal/quota/class_quotaAccount.inc:142
msgid "Hard limit for the block the user can use"
msgstr "Hardlimiet voor het blok dat de gebruiker kan toepassen"

#: personal/quota/class_quotaAccount.inc:151
msgid "Inode soft limit"
msgstr "Inode softlimiet"

#: personal/quota/class_quotaAccount.inc:151
msgid "Soft limit for the inodes the user can use"
msgstr "Softlimiet voor de inodes die de gebruiker kan toepassen"

#: personal/quota/class_quotaAccount.inc:160
msgid "Inode hard limit"
msgstr "Inode hardlimiet"

#: personal/quota/class_quotaAccount.inc:160
msgid "Hard limit for the inodes the user can use"
msgstr "Hardlimiet voor de inodes die de gebruiker kan toepassen"

#: personal/quota/class_quotaAccount.inc:169
msgid "Server"
msgstr "Server"

#: personal/quota/class_quotaAccount.inc:169
msgid "Server hosting the device this quota is for"
msgstr "Server die het toestel voor wie dit quota bestemd is host"
