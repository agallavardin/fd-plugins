# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR FusionDirectory Project
# This file is distributed under the same license as the FusionDirectory package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# fusiondirectory <contact@fusiondirectory.org>, 2018
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: FusionDirectory VERSION\n"
"Report-Msgid-Bugs-To: bugs@fusiondirectory.org\n"
"POT-Creation-Date: 2020-05-15 09:03+0000\n"
"PO-Revision-Date: 2018-08-13 20:02+0000\n"
"Last-Translator: fusiondirectory <contact@fusiondirectory.org>, 2018\n"
"Language-Team: Portuguese (https://www.transifex.com/fusiondirectory/teams/12202/pt/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: pt\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: include/class_smbHash.inc:381
msgid ""
"Your PHP install does not have the mhash() nor the hash function. Cannot do "
"MD4 hashes."
msgstr ""

#: admin/samba/class_sambaDomainManagement.inc:37
msgid "Samba domains"
msgstr ""

#: admin/samba/class_sambaDomainManagement.inc:38
msgid "Samba domain management"
msgstr ""

#: admin/samba/class_sambaDomainManagement.inc:39
msgid "Manage Samba domains"
msgstr ""

#: admin/samba/class_sambaDomain.inc:26 admin/samba/class_sambaDomain.inc:27
#: admin/samba/class_sambaDomain.inc:31
msgid "Samba Domain"
msgstr ""

#: admin/samba/class_sambaDomain.inc:32
msgid "Samba domain settings"
msgstr ""

#: admin/samba/class_sambaDomain.inc:48
#: admin/systems/samba/class_sambaSystemTab.inc:44
msgid "Properties"
msgstr "Propriedades"

#: admin/samba/class_sambaDomain.inc:51
msgid "Domain name"
msgstr ""

#: admin/samba/class_sambaDomain.inc:51
msgid "Name of this domain"
msgstr ""

#: admin/samba/class_sambaDomain.inc:55
msgid "SID"
msgstr ""

#: admin/samba/class_sambaDomain.inc:55
msgid "SID of this domain"
msgstr ""

#: admin/samba/class_sambaDomain.inc:59
msgid "Rid base"
msgstr ""

#: admin/samba/class_sambaDomain.inc:59
msgid "Algorithmic rid base"
msgstr ""

#: admin/samba/class_sambaDomain.inc:63
msgid "Minimum password age"
msgstr ""

#: admin/samba/class_sambaDomain.inc:64
msgid ""
"Minimum password age, in seconds (default: 0 => allow immediate password "
"change)"
msgstr ""

#: admin/samba/class_sambaDomain.inc:69
msgid "Maximum password age"
msgstr ""

#: admin/samba/class_sambaDomain.inc:70
msgid ""
"Maximum password age, in seconds (default: -1 => never expire passwords)"
msgstr ""

#: admin/samba/class_sambaDomain.inc:75
msgid "Next RID"
msgstr ""

#: admin/samba/class_sambaDomain.inc:76
msgid "Next NT rid to give out for anything"
msgstr ""

#: admin/samba/class_sambaDomain.inc:80
msgid "Next group RID"
msgstr ""

#: admin/samba/class_sambaDomain.inc:81
msgid "Next NT rid to give out for groups"
msgstr ""

#: admin/samba/class_sambaDomain.inc:85
msgid "Next user RID"
msgstr ""

#: admin/samba/class_sambaDomain.inc:86
msgid "Next NT rid to give our for users"
msgstr ""

#: admin/samba/class_sambaDomain.inc:90
msgid "Minimum password length"
msgstr ""

#: admin/samba/class_sambaDomain.inc:91
msgid "Minimal password length (default: 5)"
msgstr ""

#: admin/samba/class_sambaDomain.inc:95
msgid "Password history length"
msgstr ""

#: admin/samba/class_sambaDomain.inc:96
msgid "Length of Password History Entries (default: 0 => off)"
msgstr ""

#: admin/samba/class_sambaDomain.inc:100
msgid "Logon to change password"
msgstr ""

#: admin/samba/class_sambaDomain.inc:101
msgid "Force Users to logon for password change (default: 0 => off, 2 => on)"
msgstr ""

#: admin/samba/class_sambaDomain.inc:105
msgid "Lockout duration"
msgstr ""

#: admin/samba/class_sambaDomain.inc:106
msgid "Lockout duration in minutes (default: 30, -1 => forever)"
msgstr ""

#: admin/samba/class_sambaDomain.inc:110
msgid "Lockout observation window"
msgstr ""

#: admin/samba/class_sambaDomain.inc:111
msgid "Reset time after lockout in minutes (default: 30)"
msgstr ""

#: admin/samba/class_sambaDomain.inc:115
msgid "Lockout threshold"
msgstr ""

#: admin/samba/class_sambaDomain.inc:116
msgid "Lockout users after bad logon attempts (default: 0 => off)"
msgstr ""

#: admin/samba/class_sambaDomain.inc:120
msgid "Force logoff"
msgstr ""

#: admin/samba/class_sambaDomain.inc:121
msgid "Disconnect Users outside logon hours (default: -1 => off, 0 => on)"
msgstr ""

#: admin/samba/class_sambaDomain.inc:125
msgid "Refuse machine password change"
msgstr ""

#: admin/samba/class_sambaDomain.inc:126
msgid "Allow Machine Password changes (default: 0 => off)"
msgstr ""

#: admin/groups/samba/class_sambaGroup.inc:28
#: admin/systems/samba/class_sambaSystemTab.inc:29
#: config/samba/class_sambaPluginConfig.inc:27
#: personal/samba/class_sambaAccount.inc:158
msgid "Samba"
msgstr "Samba"

#: admin/groups/samba/class_sambaGroup.inc:29
msgid "Samba group settings"
msgstr ""

#: admin/groups/samba/class_sambaGroup.inc:44
#: admin/groups/samba/class_sambaGroup.inc:47
#: admin/systems/samba/class_sambaSystemTab.inc:53
#: personal/samba/class_sambaAccount.inc:201
msgid "Domain"
msgstr ""

#: admin/groups/samba/class_sambaGroup.inc:47
msgid "Samba domain"
msgstr ""

#: admin/groups/samba/class_sambaGroup.inc:51
msgid "Samba information"
msgstr ""

#: admin/groups/samba/class_sambaGroup.inc:58
msgid "Group type"
msgstr ""

#: admin/groups/samba/class_sambaGroup.inc:58
msgid "Samba group type"
msgstr ""

#: admin/groups/samba/class_sambaGroup.inc:62
#: admin/groups/samba/class_sambaGroup.inc:122
msgid "Samba group"
msgstr "Grupo samba"

#: admin/groups/samba/class_sambaGroup.inc:62
#: admin/groups/samba/class_sambaGroup.inc:122
msgid "Domain admins"
msgstr "Administradores do domínio"

#: admin/groups/samba/class_sambaGroup.inc:62
#: admin/groups/samba/class_sambaGroup.inc:122
msgid "Domain users"
msgstr "Usuários do domínio"

#: admin/groups/samba/class_sambaGroup.inc:62
#: admin/groups/samba/class_sambaGroup.inc:122
msgid "Domain guests"
msgstr "Convidados do domínio"

#: admin/groups/samba/class_sambaGroup.inc:108
msgid "Configuration error"
msgstr "Erro de configuração"

#: admin/groups/samba/class_sambaGroup.inc:108
msgid "Cannot find group SID in your configuration!"
msgstr ""

#: admin/groups/samba/class_sambaGroup.inc:122
#, php-format
msgid "Special group (%d)"
msgstr ""

#: admin/groups/samba/class_sambaGroup.inc:138
#: personal/samba/class_sambaAccount.inc:512
#: personal/samba/class_sambaAccount.inc:542
#: personal/samba/class_sambaAccount.inc:563
msgid "Warning"
msgstr "Atenção"

#: admin/groups/samba/class_sambaGroup.inc:138
#: personal/samba/class_sambaAccount.inc:512
msgid "Undefined Samba SID detected. Please fix this problem manually!"
msgstr ""

#: admin/systems/samba/class_argonautEventSambaShares.inc:27
msgid "Update Samba Shares"
msgstr ""

#: admin/systems/samba/class_sambaSystemTab.inc:30
msgid "Windows workstation information"
msgstr ""

#: admin/systems/samba/class_sambaSystemTab.inc:53
#: personal/samba/class_sambaAccount.inc:201
msgid "Samba domain name"
msgstr ""

#: config/samba/class_sambaPluginConfig.inc:28
msgid "Samba plugin configuration"
msgstr ""

#: config/samba/class_sambaPluginConfig.inc:41
#: personal/samba/class_sambaAccount.inc:159
msgid "Samba settings"
msgstr "Configurações samba"

#: config/samba/class_sambaPluginConfig.inc:44
msgid "Samba ID mapping"
msgstr ""

#: config/samba/class_sambaPluginConfig.inc:45
msgid ""
"Maintain sambaIdmapEntry objects. Depending on your setup this can "
"drastically improve the windows login performance."
msgstr ""

#: config/samba/class_sambaPluginConfig.inc:49
msgid "Samba SID"
msgstr ""

#: config/samba/class_sambaPluginConfig.inc:50
msgid ""
"A samba SID if not available inside of the LDAP though samba schema. You can"
" retrieve the current sid by net getlocalsid."
msgstr ""

#: config/samba/class_sambaPluginConfig.inc:55
msgid "Samba rid base"
msgstr ""

#: config/samba/class_sambaPluginConfig.inc:56
msgid ""
"The base id to add to ordinary sid calculations - if not available inside of"
" the LDAP though samba schema."
msgstr ""

#: config/samba/class_sambaPluginConfig.inc:61
msgid "Expiration date synchronisaton"
msgstr ""

#: config/samba/class_sambaPluginConfig.inc:61
msgid "Synchronisaton the expiration date with the POSIX one?"
msgstr ""

#: config/samba/class_sambaPluginConfig.inc:67
msgid "Generate sambaLMPassword"
msgstr ""

#: config/samba/class_sambaPluginConfig.inc:67
msgid "Needed to be compliant with Windows <= 98 or Samba < 3.2"
msgstr ""

#: config/samba/class_sambaPluginConfig.inc:71
msgid "Activate primary group warning"
msgstr ""

#: config/samba/class_sambaPluginConfig.inc:71
msgid ""
"Issue a warning if POSIX primary group cannot be converted to Samba primary "
"group when activating the Samba tab of a user"
msgstr ""

#: config/samba/class_sambaPluginConfig.inc:75
msgid "Depend upon POSIX tab"
msgstr ""

#: config/samba/class_sambaPluginConfig.inc:75
msgid "Whether the Samba user tab should depend upon the POSIX tab"
msgstr ""

#: personal/samba/class_sambaAccount.inc:31
msgid "Samba Munged Dial"
msgstr ""

#: personal/samba/class_sambaAccount.inc:185
msgid "Samba profile"
msgstr ""

#: personal/samba/class_sambaAccount.inc:191
#: personal/samba/class_sambaAccount.inc:224
msgid "Home directory drive"
msgstr ""

#: personal/samba/class_sambaAccount.inc:191
#: personal/samba/class_sambaAccount.inc:224
msgid "Letter for the home drive"
msgstr ""

#: personal/samba/class_sambaAccount.inc:196
#: personal/samba/class_sambaAccount.inc:229
msgid "Home directory path"
msgstr ""

#: personal/samba/class_sambaAccount.inc:196
#: personal/samba/class_sambaAccount.inc:229
msgid "UNC path for the home drive"
msgstr ""

#: personal/samba/class_sambaAccount.inc:205
msgid "Script path"
msgstr ""

#: personal/samba/class_sambaAccount.inc:205
msgid "Login script path"
msgstr ""

#: personal/samba/class_sambaAccount.inc:209
#: personal/samba/class_sambaAccount.inc:234
msgid "Profile path"
msgstr ""

#: personal/samba/class_sambaAccount.inc:209
#: personal/samba/class_sambaAccount.inc:234
msgid "UNC profile path"
msgstr ""

#: personal/samba/class_sambaAccount.inc:216
msgid "Terminal server"
msgstr ""

#: personal/samba/class_sambaAccount.inc:220
msgid "Allow login on terminal server"
msgstr ""

#: personal/samba/class_sambaAccount.inc:239
msgid "Inherit client config"
msgstr ""

#: personal/samba/class_sambaAccount.inc:239
msgid "Inherit client configuration"
msgstr ""

#: personal/samba/class_sambaAccount.inc:243
msgid "Initial progam"
msgstr ""

#: personal/samba/class_sambaAccount.inc:243
msgid "Program to start after connecting"
msgstr ""

#: personal/samba/class_sambaAccount.inc:247
msgid "Working directory"
msgstr ""

#: personal/samba/class_sambaAccount.inc:247
msgid "Basic working directory"
msgstr ""

#: personal/samba/class_sambaAccount.inc:251
msgid "Connection timeout"
msgstr ""

#: personal/samba/class_sambaAccount.inc:251
msgid "Timeout when connecting to terminal server"
msgstr ""

#: personal/samba/class_sambaAccount.inc:256
msgid "Disconnection timeout"
msgstr ""

#: personal/samba/class_sambaAccount.inc:256
msgid "Timeout before disconnecting from terminal server"
msgstr ""

#: personal/samba/class_sambaAccount.inc:261
msgid "Idle timeout"
msgstr ""

#: personal/samba/class_sambaAccount.inc:261
msgid "Idle timeout before disconnecting from terminal server"
msgstr ""

#: personal/samba/class_sambaAccount.inc:266
msgid "Connect client drives at logon"
msgstr ""

#: personal/samba/class_sambaAccount.inc:266
msgid "Drive to connect after login"
msgstr ""

#: personal/samba/class_sambaAccount.inc:270
msgid "Connect client printers at logon"
msgstr ""

#: personal/samba/class_sambaAccount.inc:270
msgid "Printers to connect after login"
msgstr ""

#: personal/samba/class_sambaAccount.inc:274
msgid "Default to main client printer"
msgstr ""

#: personal/samba/class_sambaAccount.inc:274
msgid "Default printer for this client"
msgstr ""

#: personal/samba/class_sambaAccount.inc:278
msgid "Shadowing"
msgstr ""

#: personal/samba/class_sambaAccount.inc:281
msgid "disabled"
msgstr "desabilitado"

#: personal/samba/class_sambaAccount.inc:281
msgid "input on, notify on"
msgstr ""

#: personal/samba/class_sambaAccount.inc:281
msgid "input on, notify off"
msgstr ""

#: personal/samba/class_sambaAccount.inc:282
msgid "input off, notify on"
msgstr ""

#: personal/samba/class_sambaAccount.inc:282
msgid "input off, nofify off"
msgstr ""

#: personal/samba/class_sambaAccount.inc:285
msgid "On broken or timed out"
msgstr ""

#: personal/samba/class_sambaAccount.inc:285
msgid "What happen if disconnected or timeout"
msgstr ""

#: personal/samba/class_sambaAccount.inc:288
msgid "disconnect"
msgstr ""

#: personal/samba/class_sambaAccount.inc:288
msgid "reset"
msgstr ""

#: personal/samba/class_sambaAccount.inc:291
msgid "Reconnect if disconnected"
msgstr ""

#: personal/samba/class_sambaAccount.inc:294
msgid "from any client"
msgstr ""

#: personal/samba/class_sambaAccount.inc:294
msgid "from previous client only"
msgstr ""

#: personal/samba/class_sambaAccount.inc:306
msgid "Access options"
msgstr ""

#: personal/samba/class_sambaAccount.inc:310
msgid "Enforce password change"
msgstr ""

#: personal/samba/class_sambaAccount.inc:310
msgid "Force the user to change his password"
msgstr ""

#: personal/samba/class_sambaAccount.inc:315
msgid "The password never expire"
msgstr ""

#: personal/samba/class_sambaAccount.inc:315
msgid "The password will never expire"
msgstr ""

#: personal/samba/class_sambaAccount.inc:320
msgid "Login from windows client requires no password"
msgstr ""

#: personal/samba/class_sambaAccount.inc:320
msgid "Login from a client without a password"
msgstr ""

#: personal/samba/class_sambaAccount.inc:325
msgid "Lock samba account"
msgstr ""

#: personal/samba/class_sambaAccount.inc:325
msgid "Lock the account"
msgstr ""

#: personal/samba/class_sambaAccount.inc:330
msgid "Cannot change password"
msgstr ""

#: personal/samba/class_sambaAccount.inc:330
msgid "Not allowed to change password"
msgstr ""

#: personal/samba/class_sambaAccount.inc:334
msgid "Account expiration"
msgstr ""

#: personal/samba/class_sambaAccount.inc:334
msgid "When does the account expire"
msgstr ""

#: personal/samba/class_sambaAccount.inc:342
msgid "Samba logon times"
msgstr ""

#: personal/samba/class_sambaAccount.inc:342
msgid "What is the allowed time to connect"
msgstr ""

#: personal/samba/class_sambaAccount.inc:343
msgid "Edit settings"
msgstr ""

#: personal/samba/class_sambaAccount.inc:359
msgid "System trust"
msgstr "Sistema de confiança"

#: personal/samba/class_sambaAccount.inc:365
msgid "Allow connection from these workstations only"
msgstr ""

#: personal/samba/class_sambaAccount.inc:365
msgid "Only allow this user to connect to this list of hosts"
msgstr ""

#: personal/samba/class_sambaAccount.inc:480
#: personal/samba/class_sambaAccount.inc:487
#, php-format
msgid "Field \"%s\" is required when \"%s\" is filled! Fill both field or empty them."
msgstr ""

#: personal/samba/class_sambaAccount.inc:495
msgid "The windows user manager allows eight clients at maximum!"
msgstr ""

#: personal/samba/class_sambaAccount.inc:543
msgid ""
"Cannot convert primary group to samba group: group cannot be identified!"
msgstr ""

#: personal/samba/class_sambaAccount.inc:564
msgid ""
"Cannot convert primary group to samba group: group samba tab is disabled!"
msgstr ""

#: personal/samba/sambaLogonHours.tpl.c:2
msgid "Specify the hours this user is allowed to log in"
msgstr ""

#: personal/samba/sambaLogonHours.tpl.c:5
msgid "Hour"
msgstr ""
