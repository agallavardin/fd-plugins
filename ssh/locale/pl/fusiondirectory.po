# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR FusionDirectory Project
# This file is distributed under the same license as the FusionDirectory package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: FusionDirectory VERSION\n"
"Report-Msgid-Bugs-To: bugs@fusiondirectory.org\n"
"POT-Creation-Date: 2020-05-15 09:03+0000\n"
"PO-Revision-Date: 2018-08-13 20:03+0000\n"
"Language-Team: Polish (https://www.transifex.com/fusiondirectory/teams/12202/pl/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: pl\n"
"Plural-Forms: nplurals=4; plural=(n==1 ? 0 : (n%10>=2 && n%10<=4) && (n%100<12 || n%100>14) ? 1 : n!=1 && (n%10>=0 && n%10<=1) || (n%10>=5 && n%10<=9) || (n%100>=12 && n%100<=14) ? 2 : 3);\n"

#: personal/ssh/class_sshAccount.inc:61
msgid "Upload error"
msgstr ""

#: personal/ssh/class_sshAccount.inc:61
msgid "Unknown public key format!"
msgstr ""

#: personal/ssh/class_sshAccount.inc:81
msgid "SSH"
msgstr ""

#: personal/ssh/class_sshAccount.inc:82
msgid "Edit user's SSH public keys"
msgstr ""

#: personal/ssh/class_sshAccount.inc:100
msgid "SSH Keys"
msgstr ""

#: personal/ssh/class_sshAccount.inc:106
msgid "SSH public keys for this user"
msgstr ""

#: personal/ssh/class_sshAccount.inc:130
#, php-format
msgid "Error : there are several keys with fingerprint %s"
msgstr ""
